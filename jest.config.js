// module.exports = {
//   testEnvironment: 'jsdom',
//   testPathIgnorePatterns: ['/node_modules', '/.expo/'],
//   collectCoverage: true,
//   collectCoverageFrom: ['src/**/*.ts(x)']
//   // setupFilesAfterEnv: ['<rootDir>/.jest.setup.ts']
// }

module.exports = {
  preset: 'react-native',
  moduleFileExtensions: ['ts', 'tsx', 'js', 'jsx', 'json', 'node']
}
