# Usando Docker

### Instalação
```
./rodar.sh
```

# Sem Docker

### É necessario ter node instalado!

### Instalação
```
npm install
npm install -g yarn
npm install --global expo-cli
```

### Executar (uma dessas opções)
```
expo start (usando app expo)
yarn ios
yarn android
```