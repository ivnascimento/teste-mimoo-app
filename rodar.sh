#!/bin/bash

echo "Iniciando..."

if [ -f package.json ]; then
    
    docker run -it --rm -p 19000:19000 -p 19001:19001 -p 19002:19002 -v "$PWD:/app" \
-e REACT_NATIVE_PACKAGER_HOSTNAME=192.168.0.119 \
-e EXPO_DEVTOOLS_LISTEN_ADDRESS=0.0.0.0 \
--name=expo kerbe/expo start

fi

echo "Fim - Instalado e rodando!"