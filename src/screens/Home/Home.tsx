import React from 'react'
import { StackNavigationProp } from '@react-navigation/stack'
import * as S from './styles'

type MyProfileScreenProps = {
  navigation: StackNavigationProp
};

export default class Home extends React.Component<MyProfileScreenProps> {

  render() {
    return (
      <S.Container>
        <S.ContainerScroll>
          <S.Logo resizeMode="contain" source={require('./../../img/logo.png')}/>
  
          <S.Titulo>Bem vindo à {"\n"}Mimoo!</S.Titulo>
  
          <S.Descricao>Alegre. Divertido. Relevante. {"\n"}Você vai adorar!</S.Descricao>
  
          <S.DescricaoBlack>Nos conte um pouco sobre você {"\n"}e ganhe pontos!</S.DescricaoBlack>
  
          <S.Fila>
            <S.FilaImg resizeMode="contain" source={require('./../../img/fila-home.png')}/>
          </S.Fila>
  
        </S.ContainerScroll>
  
        <S.BotaoComecar onPress={() => this.props.navigation.navigate('NameForm')} >
          <S.TextComecar>Começar</S.TextComecar>
        </S.BotaoComecar>
  
      </S.Container>
    )
  }

}
