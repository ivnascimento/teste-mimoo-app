import styled from 'styled-components/native'
import { Image } from 'react-native'
import Constants from 'expo-constants'

export const Container = styled.SafeAreaView`
  flex: 1;
  margin-top: ${Constants.statusBarHeight};
  background-color: ${props => props.theme.colors.background};
`;

export const ContainerScroll = styled.ScrollView`
  flex: 1;
  padding: 0px 20px;
`;

export const Logo = styled(Image)`
  margin-top: 10px;
  max-width: 150px;
`;

export const Titulo = styled.Text`
  font-weight: bold;
  font-size: 35px;
  line-height: 53px;
  color: ${props => props.theme.colors.primary};
`;

export const Descricao = styled.Text`
  margin-top: 20px;
  font-weight: bold;
  font-size: 20px;
  line-height: 30px;
  color: ${props => props.theme.colors.primary};
`;

export const DescricaoBlack = styled.Text`
  margin-top: 20px;
  font-size: 20px;
  line-height: 30px;
  color: #000000;
`;

export const Fila = styled.View`
  position: relative;
`;

export const FilaImg = styled(Image)`
  left: 40%;
  position: absolute;
  top: -80px;
  max-width: 500px;
`;

export const BotaoComecar = styled.TouchableOpacity`
  background-color: ${props => props.theme.colors.primary};
  padding: 15px;
  align-items: center;
`

export const TextComecar = styled.Text`
  font-weight: bold;
  font-size: 20px;
  line-height: 28px;
  color: #ffffff;
`;