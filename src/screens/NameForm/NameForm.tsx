import React, { useState, useEffect } from 'react'
import { Alert } from "react-native"
import { AsyncStorage } from 'react-native'
import * as S from './styles'

export default function NameForm(props: any) {
  const [nome, setNome] = useState('')

  useEffect(() => {
      async function getNome () {
        await AsyncStorage.getItem("NomeUsuario").then((value: any) => {
          if (value) setNome(value)
       })
      }
  
      getNome()
  }, [])

  function Continuar () {
    if (nome && nome.length > 0 ) {
      AsyncStorage.setItem('NomeUsuario', nome)
      props.navigation.navigate('Produto')
    } else {
      Alert.alert("Informe seu Nome", "Você tem que digitar seu nome para continuar!")
    }
  }

  return (
    <S.Container>

      <S.ContainerScroll>

        <S.Titulo>Informe seu nome</S.Titulo>

        <S.Input
          onChangeText={text => setNome(text)}
          value={nome}
        />

      </S.ContainerScroll>

      <S.BotaoContinuar onPress={() => Continuar()}>
        <S.TextContinuar>Continuar</S.TextContinuar>
      </S.BotaoContinuar>

    </S.Container>
  )

}
