import styled from 'styled-components/native'

export const Container = styled.SafeAreaView`
  flex: 1;
  background-color: ${props => props.theme.colors.background};
`;

export const ContainerScroll = styled.ScrollView`
  flex: 1;
  padding: 0px 20px;
`;


export const Titulo = styled.Text`
  padding: 30px 0px;
  font-weight: 600;
  font-size: 30px;
  line-height: 37px;
  color: #000000;
`;

export const Input = styled.TextInput`
  border-bottom-color: ${props => props.theme.colors.primary};
  border-bottom-width: 1;
  font-size: 22px;
  line-height: 24px;
  height: 30px;
`

export const BotaoContinuar = styled.TouchableOpacity`
  background-color: ${props => props.theme.colors.primary};
  padding: 15px;
  align-items: center;
`

export const TextContinuar  = styled.Text`
  font-weight: bold;
  font-size: 20px;
  line-height: 28px;
  color: #ffffff;
`;









