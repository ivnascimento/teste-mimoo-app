import React, { useState, useEffect } from 'react'
import { AsyncStorage } from 'react-native'
import { Alert } from "react-native"
import * as S from './styles'

const axios = require('axios');

export default function ProdutoAdd(props: any) {
  const [codigoBarras, setCodigoBarras] = useState('')
  const [produto, setProduto] = useState(null)
  const [pontos, setPontos] = useState('100')

  useEffect(() => {
    const codigoBarras = props.route && props.route.params && props.route.params.codigoBarras ? props.route.params.codigoBarras : null
    if (codigoBarras) setCodigoBarras(codigoBarras)

    function getProdutoCodigoBarras () {
      axios.get('https://virtserver.swaggerhub.com/mimoo-tech/frontend-challenge-api/1.0.0/products/' + codigoBarras)
        .then(function (res: Object) {
          if (res && res.data) setProduto(res.data)
        })
        .catch(function (error: Object) {
          Alert.alert("Tente novamente mais tarde!", "Erro ao Buscar na Base de Dados!")
          console.log(error)
        })
    }

    async function getPontos () {
      await AsyncStorage.getItem("PontosUsuario").then((value: any) => {
        if (value) setPontos(value)
      })
    }

    getPontos()
    getProdutoCodigoBarras()
  }, [])

  function Salvar () {
    const newPontos: number = parseInt(pontos) + 100

    AsyncStorage.setItem('PontosUsuario', newPontos.toString())
    props.navigation.navigate('Produto', { pontos: newPontos.toString()} )
    Alert.alert("Salvo com sucesso!")
  }

  if (produto) {
    return (
      <S.Container>

        <S.ContainerScroll>
          <S.Div>

            <S.TextPrincipal>Identificamos que você consome</S.TextPrincipal>

            <S.TextNomeProduto>{produto.name}</S.TextNomeProduto>

            <S.ImgContainer>
              <S.ImgProduto source={{ uri: produto.image ? produto.image : null }}/>
            </S.ImgContainer>

            <S.TextBlackBold>Parabéns {"\n"}Você ganhou 100 pontos!</S.TextBlackBold>
            <S.TextMediumBold>Continue para ganhar ainda mais pontos</S.TextMediumBold>

          </S.Div>
        </S.ContainerScroll>

        <S.BotaoSalvar onPress={() => Salvar()} >
          <S.TextSalvar>Salvar</S.TextSalvar>
        </S.BotaoSalvar>

      </S.Container>
    )
  } else {
    return (
      <S.Container>
        <S.TextPrincipal>Carregando...</S.TextPrincipal>
      </S.Container>
    )
  }

}
