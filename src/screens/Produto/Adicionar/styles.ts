import styled from 'styled-components/native'
import { Image } from 'react-native'

export const Container = styled.SafeAreaView`
  flex: 1;
  background-color: #DFBF94;
`;

export const ContainerScroll = styled.ScrollView`
  flex: 1;
  padding: 30px 50px;
`;

export const Div = styled.View`
  flex-direction: column;
  align-items: center;
`

export const TextPrincipal = styled.Text`
  font-weight: 600;
  font-size: 25px;
  line-height: 38px;
  color: white;
  text-align: center;
`;

export const TextNomeProduto = styled.Text`
  font-weight: bold;
  font-size: 34px;
  line-height: 51px;
  color: white;
  text-align: center;
  padding-top: 10px;
  padding-bottom: 20px;
`;

export const ImgContainer = styled.View`
  width: 100%;
  padding-top: 100%;
`;


export const ImgProduto = styled(Image)`
  position: absolute;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  resize-mode: contain;
`;

export const TextBlackBold = styled.Text`
  font-weight: bold;
  font-size: 20px;
  line-height: 30px;
  color: black;
  text-align: center;
  padding-top: 20px;
`;

export const TextMediumBold = styled.Text`
  font-weight: 600;
  font-size: 20px;
  line-height: 30px;
  color: black;
  text-align: center;
  padding-top: 10px;
`;

export const BotaoSalvar = styled.TouchableOpacity`
  background-color: ${props => props.theme.colors.background};
  padding: 15px;
  align-items: center;
`

export const TextSalvar = styled.Text`
  font-weight: bold;
  font-size: 20px;
  line-height: 28px;
  color: ${props => props.theme.colors.primary};;
`;