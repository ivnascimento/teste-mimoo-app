import styled from 'styled-components/native'

export const Container = styled.View`
  flex: 1;
  flex-direction: column;
  justify-content: flex-end;
`;

export const TextPermissao = styled.Text`
  font-size: 25px;
  color: black;
  text-align: center;
  padding-top: 50px;
`;

export const CardResultado = styled.View`
  background-color: rgba(255, 255, 255, 0.5);
  height: 100px;
  padding: 10px 20px;
`

export const TituloResultado = styled.Text`
  font-weight: 600;
  font-size: 18px;
  line-height: 38px;
  color: white;
`;

export const CodigoResultado = styled.Text`
  font-weight: 600;
  font-size: 25px;
  line-height: 38px;
  color: white;
`;

export const BotaoConfirmar = styled.TouchableOpacity`
  background-color: ${props => props.theme.colors.background};
  padding: 15px;
  align-items: center;
`

export const TextConfirmar  = styled.Text`
  font-weight: bold;
  font-size: 20px;
  line-height: 28px;
  color: ${props => props.theme.colors.primary};;
`;