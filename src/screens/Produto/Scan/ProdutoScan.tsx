import React, { useState, useEffect } from 'react'
import { Alert } from "react-native"
import { StyleSheet } from 'react-native'
import { Entypo } from '@expo/vector-icons';
import { BarCodeScanner } from 'expo-barcode-scanner'
import * as S from './styles'

export default function ProdutoScan(props: any) {
  const [hasPermission, setHasPermission] = useState(null)
  const [codigo, setCodigo] = useState('')

  useEffect(() => {
    (async () => {
      const { status } = await BarCodeScanner.requestPermissionsAsync()
      setHasPermission(status === 'granted')
    })()
  }, [])

  // Response Codigo de Barras
  const handleBarCodeScanned = (res: Object) => {
    setCodigo(res.data)
  }

  // Aguardando Usuario aceitar solicitação de Camera
  if (hasPermission === null) {
    return <S.TextPermissao>Solicitando permissão de câmera...</S.TextPermissao>
  }

  // Usuario recusou aceitar premissao de camera
  if (hasPermission === false) {
    return (
      <>
        <S.TextPermissao>
          Sem acesso à câmera!
        </S.TextPermissao>
        <S.TextPermissao>
          <Entypo name="emoji-sad" size={100} />
        </S.TextPermissao>
      </>
    )
  }

  function Continuar () {
    if (codigo && codigo.length > 0 ) {
      props.navigation.navigate('ProdutoAdd', { codigoBarras: codigo })
    } else {
      Alert.alert("Nenhum código escaneado!", "Aproxime a camera no código de barras do produto!")
    }
  }

  return (
    <S.Container>
      
      <BarCodeScanner
        onBarCodeScanned={handleBarCodeScanned}
        style={StyleSheet.absoluteFillObject}
      />

      <S.CardResultado>
        <S.TituloResultado>Número do código de barras:</S.TituloResultado>
        <S.TituloResultado>{codigo && codigo.length > 0 ? "" : "Aproxime a camera no código de barras!"}</S.TituloResultado>
        <S.CodigoResultado>{codigo && codigo.length > 0 ? codigo : ""}</S.CodigoResultado>
      </S.CardResultado>

      <S.BotaoConfirmar onPress={() => Continuar()} >
        <S.TextConfirmar>Confirmar</S.TextConfirmar>
      </S.BotaoConfirmar>
  
    </S.Container>
  )
}
