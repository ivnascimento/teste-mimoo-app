import styled from 'styled-components/native'
import { Image } from 'react-native'

export const Container = styled.SafeAreaView`
  flex: 1;
  background-color: ${props => props.theme.colors.background};
`;

export const ContainerScroll = styled.ScrollView`
  flex: 1;
  padding: 20px;
`;

export const Div = styled.View`
  flex: 1;
  position: relative;
  padding-bottom: 40px;
`

export const Titulo = styled.Text`
  font-weight: 600;
  font-size: 34px;
  line-height: 37px;
  color: ${props => props.theme.colors.secundary};
  padding-bottom: 10px;
`;

export const DescricaoTitulo = styled.Text`
  font-size: 16px;
  line-height: 25px;
  color: ${props => props.theme.colors.secundary};
`;

export const PontosText = styled.Text`
  font-weight: 600;
  font-size: 18px;
  line-height: 18px;
  color: #482F05;
  padding-top: 20px;
`;

export const PontosValue = styled.Text`
  font-weight: bold;
  font-size: 25px;
  line-height: 25px;
  color: #482F05;
  padding: 5px 15px;
`;

export const Marca = styled.Text`
  font-weight: 600;
  font-size: 18px;
  color: #949494;
  padding-top: 20px;
  padding-bottom: 15px;
`

export const Row = styled.View`
  flex-direction: row;
  display: flex;
  flex-wrap: wrap;
`

export const Col = styled.View`
  flex: 0 50%;
`

export const Card = styled.View(props => ({
  backgroundColor: props && props.blue ? '#abc3ce': '#ceb5ab',
  marginRight: 10,
  marginBottom: 10,
  minHeight: 150,
  borderRadius: 5
}))

export const Add = styled.TouchableOpacity`
  position: absolute;
  right: 0px;
  bottom: 20px;
  width: 100px;
  height: 100px;
`

export const Tabs = styled.View`
  flex-direction: row;
  padding-top: 10px;
  padding-bottom: 20px;
`

export const Tab = styled.View`
  padding-right: 10px;
`

export const TabButton = styled.TouchableOpacity(props => ({
  paddingBottom: 5,
  borderBottomWidth: props && props.ativo ? 3: 0,
  borderBottomColor: props && props.ativo ? props.theme.colors.primary: '#949494'
}))

export const TabText = styled.Text(props => ({
  color: props && props.ativo ? props.theme.colors.primary: '#949494',
  fontWeight: 'bold',
  fontSize: 20,
}))

export const ImgProduto = styled(Image)`
  min-height: 150px;
`;