import React, { useState, useEffect } from 'react'
import { View } from 'react-native'
import { MaterialIcons, SimpleLineIcons, AntDesign } from '@expo/vector-icons'
import { AsyncStorage } from 'react-native'
import Themes from '../../../constants/themes'
import * as S from './styles'

const axios = require('axios')

export default function ProdutoLista(props: any) {
  const [nomeUsuario, setNomeUsuario] = useState('')
  const [pontos, setPontos] = useState('100')
  const [dados, setDados] = useState([])
  const [categoriaSelecionada, setCategoria] = useState(null)

  useEffect(() => {
    // Get Lista de Produtos
    function getLista () {
      axios.get('https://virtserver.swaggerhub.com/mimoo-tech/frontend-challenge-api/1.0.0/products')
        .then(function (res: Object) {
          if (res && res.data && res.data.length > 0) {
            setDados(res.data)
            setCategoria(res.data[0])
          }
        })
        .catch(function (error: Object) {
          console.log(error);
        })
    }

    // Get Nome Usuario
    async function getNome () {
      await AsyncStorage.getItem("NomeUsuario").then((value: any) => {
        if (value) setNomeUsuario(value)
      })
    }

    // Get Pontos do Usuario
    async function getPontos () {
      await AsyncStorage.getItem("PontosUsuario").then((value: any) => {
        if (value) setPontos(value)
      })
    }

    getNome()
    getPontos()
    getLista()
  }, [])

  function listaProdutosRender () {

    if (categoriaSelecionada && categoriaSelecionada.brands) {
      const marcas = categoriaSelecionada.brands

      return (
        <>
          {marcas.map((marca: Object, key) => (
            <View key={key}>
              <S.Marca>
                <SimpleLineIcons name="location-pin" size={22} color={'#949494'} /> {marca.name}
              </S.Marca>

              <S.Row>
                { marca.products.map((produto: Object, index) => (
                  <S.Col key={index}>
                    <S.Card blue={categoriaSelecionada.category === 'Snacks' ? true : false}>
                      <S.ImgProduto
                        resizeMode="contain"
                        source={{ uri: produto ? produto.image : null}}
                      />
                    </S.Card>
                  </S.Col>
                ))}

              </S.Row>

            </View>
          ))}
        </>
      )
    } else return (
      <S.Titulo>Carregando...</S.Titulo>
    )
  }

  return (
    <S.Container>
      
      <S.ContainerScroll>

        <S.Titulo>Olá {nomeUsuario}!</S.Titulo>

        <S.DescricaoTitulo>Adicione mais produtos à sua lista {"\n"}e ganhe pontos!</S.DescricaoTitulo>

        <S.PontosText>
          <AntDesign name="star" size={18} color={'#482F05'} /> Pontos
        </S.PontosText>

        <S.PontosValue>{pontos}</S.PontosValue>

        <S.Tabs>
          {dados.map((item, i) => (
            <S.Tab key={i}>
              <S.TabButton 
                ativo={categoriaSelecionada && categoriaSelecionada.category === item.category ? true : null} 
                onPress={() => setCategoria(item)}
              >
                <S.TabText ativo={categoriaSelecionada && categoriaSelecionada.category === item.category ? true : null} >
                  {item.category}
                </S.TabText>
              </S.TabButton>
            </S.Tab>
          ))}
        </S.Tabs>

        <S.Div>
          {listaProdutosRender()}
        </S.Div>

      </S.ContainerScroll>

      <S.Add onPress={() => props.navigation.navigate('ProdutoScan')}>
        <MaterialIcons name="add-circle" size={80} color={Themes.colors.primary} />
      </S.Add>

    </S.Container>
  )

}
