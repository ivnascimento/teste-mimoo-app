import React, { useEffect } from 'react'
import { ThemeProvider } from 'styled-components'
import { NavigationContainer } from '@react-navigation/native'
import { createStackNavigator } from '@react-navigation/stack'
import { StatusBar } from 'expo-status-bar'
import * as Updates from 'expo-updates'
import theme from './constants/themes'

// Screens
import HomeScreen from './screens/Home/Home'
import NameFormScreen from './screens/NameForm/NameForm'
import ProdutoScreen from './screens/Produto/Lista/ProdutoLista'
import ProdutoScanScreen from './screens/Produto/Scan/ProdutoScan'
import ProdutoAddScreen from './screens/Produto/Adicionar/ProdutoAdd'

// Components
import MenuButton from './components/MenuButton/MenuButton'
import MenuTitle from './components/MenuTitle/MenuTitle'

function updateJsApp() {
  useEffect(() => {
    async function updateApp() {
      const { isAvailable } = await Updates.checkForUpdateAsync()

      if (isAvailable) {
        await Updates.fetchUpdateAsync()
        await Updates.reloadAsync()
      }
    }

    updateApp()
  }, [])
}
const Stack = createStackNavigator()


function MyStack() {
  return (
    <Stack.Navigator 
      initialRouteName="Home"
      screenOptions={{
        title: '',
        headerBackTitleVisible: false,
        headerBackImage: (props) => <MenuButton {...props} />,
        headerStyle: {
          shadowColor: 'transparent'
        }
      }}
    >

      <Stack.Screen 
        name="Home" 
        component={HomeScreen}
        options={{
          headerShown: false
        }}
      />

      <Stack.Screen 
        name="NameForm" 
        component={NameFormScreen}
      />

      <Stack.Screen 
        name="Produto" 
        component={ProdutoScreen}
        options={{
          headerShown: false
        }}
      />

      <Stack.Screen 
        name="ProdutoScan" 
        component={ProdutoScanScreen}
        options={{
          headerTitle: (props) => <MenuTitle {...props } title="Escanear Produto" />
        }}
      />

      <Stack.Screen 
        name="ProdutoAdd" 
        component={ProdutoAddScreen}
        options={{
          headerShown: false
        }}
      />
      
    </Stack.Navigator>
  )
}

export default function App() {
  updateJsApp()
  return (
    <ThemeProvider theme={theme}>
      <NavigationContainer>
        <MyStack />
        <StatusBar style="dark"/>
      </NavigationContainer>
    </ThemeProvider>
  )
}