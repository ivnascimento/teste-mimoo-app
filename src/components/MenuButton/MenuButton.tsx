import React from 'react'
import { Ionicons } from '@expo/vector-icons';
import Themes from './../../constants/themes'
import * as S from './styles'

export default function MenuButton(props: any) {
  return (
    <S.Container>
      <Ionicons name="md-arrow-round-back" size={32} color={ Themes.colors.primary } />
    </S.Container>
  )
}
