import styled from 'styled-components/native'

export const Container = styled.View`
  background-color: ${props => props.theme.colors.background};
`;

export const Text = styled.Text`
  font-size: 23px;
  line-height: 29px;
  color: #3A3A3A;
  opacity: 0.47;
`;
