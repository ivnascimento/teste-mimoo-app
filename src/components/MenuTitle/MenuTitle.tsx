import React from 'react'
import * as S from './styles'

export default function MenuTitle(props: any) {
  return (
    <S.Container>
      <S.Text>{props.title}</S.Text>
    </S.Container>
  )
}
